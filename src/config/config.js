export const navbarBrand = "Berita";
export const header = "";
export const navs = [
    { nav: "Beranda", page: "/" },
    { nav: "Bisnis", page: "/bisnis" },
    { nav: "Olahraga", page: "/olahraga" },
    { nav: "Sains", page: "/sains" },
    { nav: "Kesehatan", page: "/kesehatan" },
    { nav: "Hiburan", page: "/hiburan" },
    { nav: "Teknologi", page: "/teknologi" }
]

export const router = [
    { path: "/", key: "general", category: "general", country: "id" },
    { path: "/bisnis", key: "business", category: "business", country: "id" },
    { path: "/olahraga", key: "sports", category: "sports", country: "id" },
    { path: "/sains", key: "science", category: "science", country: "id" },
    { path: "/kesehatan", key: "health", category: "health", country: "id" },
    { path: "/hiburan", key: "entertainment", category: "entertainment", country: "id" },
    { path: "/teknologi", key: "technology", category: "technology", country: "id" }
]

export const summary = "Author, Sumber dan Rilis";
export const author = (author) => `Author: ${!author ? "Unknown" : author}`;
export const channel = (channel) => `Sumber: ${channel}`;
export const lastUpdate = (date) => `Rilis: ${new Date(date).toGMTString()}`;
