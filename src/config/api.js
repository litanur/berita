export const API_DOMAIN = "https://newsapi.org/v2/top-headlines?country="
export const API_KEY = "92005fe57f854d0a9b78f168c9e9a478"
export const endpointPath = (country, category, page, pageSize) => `${API_DOMAIN}${country}&category=${category}&apiKey=${API_KEY}&page=${page}&pageSize=${pageSize}`;